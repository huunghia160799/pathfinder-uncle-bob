from django.urls import path

from .views import TestAction #basic action like get,post,put,delete

from .views import TestFunction #high level action like get all


app_name = "Test"

# app_name will help us do a reverse look-up latter.
urlpatterns = [
    #api getting ad with id_ad params
    path('tests/<str:test_id_input>/',TestAction.as_view()),
    path('tests/',TestAction.as_view()),
    path('tests/get_all/get/<str:skill_title_input>/',TestFunction.as_view())
]