from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db.models import F,ExpressionWrapper,FloatField,Count
import pandas as pd
import json
from django.db.models.functions import Floor
from django.http import JsonResponse
from .models import Test#,Category
from Pathway.Skill.models import Skill
from datetime import datetime
from rest_framework import status
import math
class TestAction(APIView):
    def get(self,request,test_id_input):
        #get Test by id
        print("Test ID",test_id_input)
        test = Test.objects.filter(id=test_id_input).values()
        if(test.exists()==False):
            print("here")
            return Response({"data":[]})
        return Response({"data": test[0]})
    def post(self,request):
        #adding new Test 
        Test_request = request.POST
        Test_insert = {}
        # if(Test.objects.filter(id=Test_insert['test_id']).values().exists()):
        #     return Response({"This Test ID has been used by others"})
        Test_insert['title'] = request.POST["title"]
        Test_insert['description'] = request.POST["description"]
        Test_insert['unit_test'] = request.POST["unit_test"]
        Test_insert['sample_solution'] = request.POST["sample_solution"]
        if(Skill.objects.filter(title=request.POST["skill_title"]).values().exists()==False):
            return Response({"This Skill title does not exist"},status=status.HTTP_400_BAD_REQUEST)
        skill = Skill.objects.get(title=request.POST["skill_title"])
        Test_insert['skill'] = skill
        new_Test = Test.objects.create(**Test_insert)
        del Test_insert['skill']
        Test_insert['skill_id'] = request.POST["skill_title"]
        return Response({"data":Test_insert},status=status.HTTP_201_CREATED)
    def put(self,request):
        Test_insert = {}
        Test_insert['id'] = request.POST["test_id"]
        Test_insert['title'] = request.POST["title"]
        Test_insert['description'] = request.POST["description"]
        Test_insert['unit_test'] = request.POST["unit_test"]
        Test_insert['sample_solution'] = request.POST["sample_solution"]
        if(Test.objects.filter(id=request.POST["test_id"]).values().exists()==False):
            return Response({"Non-existing Test"},status=status.HTTP_400_BAD_REQUEST)
        if(Skill.objects.filter(title=request.POST["skill_title"]).values().exists()==False):
            return Response({"This Skill title does not exist"},status=status.HTTP_400_BAD_REQUEST)
        skill = Skill.objects.get(title=request.POST["skill_title"])
        Test_insert['skill'] = skill
        match_Test = Test.objects.get(id=request.POST["test_id"])
        match_Test.title = Test_insert['title']
        match_Test.description = Test_insert['description']
        match_Test.unit_test = Test_insert['unit_test']
        match_Test.sample_solution = Test_insert['sample_solution']
        match_Test.skill = Test_insert['skill']
        match_Test.save()
        del Test_insert['skill']
        Test_insert['skill_title'] = request.POST["skill_title"]
        return Response({"data":Test_insert},status=status.HTTP_201_CREATED)
    def delete(self,request,test_id_input):
        print("Test ID for deleting:",test_id_input)
        if(Test.objects.filter(id=test_id_input).values().exists()==False):
            return Response({"Non-existing Test"},status=status.HTTP_400_BAD_REQUEST)
        Test_delete = Test.objects.get(id=test_id_input)
        Test_delete.delete()
        return Response({"Deleting Test {} successfully".format(test_id_input)},status=status.HTTP_202_ACCEPTED)
# Create your views here.
class TestFunction(APIView):
    def get(self,request,skill_title_input):#get all test of a skill
        all_test_of_skill = Test.objects.filter(skill=skill_title_input).values()
        return Response({"All test of {} skill".format(skill_title_input):list(all_test_of_skill)})