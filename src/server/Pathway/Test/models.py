from django.db import models

from Pathway.Skill.models import Skill
class Test(models.Model):
  #test_id = models.IntegerField(primary_key=True)
  title = models.CharField(max_length=255,null=True)
  description = models.CharField(max_length=255,null=True)
  unit_test = models.CharField(max_length=255,null=True)
  sample_solution = models.CharField(max_length=255,null=True)
  skill = models.ForeignKey(Skill, on_delete=models.CASCADE)
# Create your models here.
