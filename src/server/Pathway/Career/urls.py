from django.urls import path

from .views import CareerAction#basic activity like get,post,put,delete

from .views import CareerFunction# high level activity like get all...
app_name = "Career"

# app_name will help us do a reverse look-up latter.
urlpatterns = [
    #api getting ad with id_ad params
    path('careers/<str:career_title_input>/',CareerAction.as_view()),
    path('careers/',CareerAction.as_view()),
    path('careers/get_all/get/',CareerFunction.as_view())
]