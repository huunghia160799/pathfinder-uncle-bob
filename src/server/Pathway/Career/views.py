from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from django.db.models import F,ExpressionWrapper,FloatField,Count
import pandas as pd
import json
from django.db.models.functions import Floor
from django.http import JsonResponse
from .models import Career#,Category
from datetime import datetime
import math
class CareerAction(APIView):
    def get(self,request,career_title_input):
        #get career by id
        print("Career Title",career_title_input)
        career = Career.objects.filter(title=career_title_input).values()
        if(career.exists()==False):
            print("here")
            return Response({"data":[]})
        return Response({"data": career[0]})
    def post(self,request):
        #adding new career 
        career_request = request.POST
        career_insert = {}
        career_insert['title'] = request.POST["title"]
        career_insert['isUpdated'] = request.POST["isUpdated"]
        if(Career.objects.filter(title=career_insert['title']).values().exists()):
            return Response({"This career title has been used by others"},status=status.HTTP_400_BAD_REQUEST)
        career_insert['description'] = request.POST["description"]
        new_career = Career.objects.create(**career_insert)
        return Response({"data":career_insert},status=status.HTTP_201_CREATED)
    def put(self,request):
        career_insert = {}
        career_insert['description'] = request.POST["description"]
        career_insert['isUpdated'] = request.POST["isUpdated"]
        if(Career.objects.filter(title=request.POST["title"]).values().exists()==False):
            return Response({"Non-existing career"},status=status.HTTP_400_BAD_REQUEST)
        match_career = Career.objects.get(title=request.POST["title"])
        career_insert['title'] = match_career.title
        match_career.description = career_insert['description']
        match_career.save()
        return Response({"data":career_insert},status=status.HTTP_201_CREATED)
    def delete(self,request,career_title_input):
        print("career title for deleting:",career_title_input)
        if(Career.objects.filter(title=career_title_input).values().exists()==False):
            return Response({"Non-existing career"},status=status.HTTP_400_BAD_REQUEST)
        career_delete = Career.objects.get(title=career_title_input)
        career_delete.delete()
        return Response({"Deleting successfully"},status=status.HTTP_202_ACCEPTED)
class CareerFunction(APIView):
    def get(self,request): 
        all_career = Career.objects.all()
        return Response({"data":list(all_career.values())})
