from django.urls import path

from .views import SkillAction #basic action like get,post,put,delete

from .views import SkillFunction #high level action like get all ...
app_name = "Skill"

# app_name will help us do a reverse look-up latter.
urlpatterns = [
    #api getting ad with id_ad params
    path('skills/<str:skill_title_input>/',SkillAction.as_view()),
    path('skills/',SkillAction.as_view()),
    path('skills/get_all/get/',SkillFunction.as_view())
]