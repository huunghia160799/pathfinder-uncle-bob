from django.shortcuts import render

from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db.models import F,ExpressionWrapper,FloatField,Count
import pandas as pd
import json
from rest_framework import status
from django.db.models.functions import Floor
from django.http import JsonResponse
from .models import Skill#,Category
from datetime import datetime
import math
class SkillAction(APIView):
    def get(self,request,skill_title_input):
        #get Skill by id
        print("Skill Title",skill_title_input)
        skill = Skill.objects.filter(title=skill_title_input).values()
        if(skill.exists()==False):
            print("here")
            return Response({"data":[]})
        return Response({"data": skill[0]})
    def post(self,request):
        #adding new Skill 
        skill_request = request.POST
        skill_insert = {}
        skill_insert['title'] = request.POST["title"]
        if(Skill.objects.filter(title=skill_insert['title']).values().exists()):
            return Response({"This Skill title has been used by others"},status=status.HTTP_400_BAD_REQUEST)
        skill_insert['description'] = request.POST["description"]
        new_skill = Skill.objects.create(**skill_insert)
        return Response({"data":skill_insert},status=status.HTTP_201_CREATED)
    def put(self,request):
        Skill_insert = {}
        Skill_insert['description'] = request.POST["description"]
        if(Skill.objects.filter(title=request.POST["title"]).values().exists()==False):
            return Response({"Non-existing Skill"},status=status.HTTP_400_BAD_REQUEST)
        match_Skill = Skill.objects.get(title=request.POST["title"])
        Skill_insert['title'] = match_Skill.title
        match_Skill.description = Skill_insert['description']
        match_Skill.save()
        return Response({"data":Skill_insert},status=status.HTTP_201_CREATED)
    def delete(self,request,skill_title_input):
        print("Skill title for deleting:",skill_title_input)
        if(Skill.objects.filter(title=skill_title_input).values().exists()==False):
            return Response({"Non-existing Skill"},status=status.HTTP_400_BAD_REQUEST)
        Skill_delete = Skill.objects.get(title=skill_title_input)
        Skill_delete.delete()
        return Response({"Deleting Skill {} successfully".format(skill_title_input)},status=status.HTTP_202_ACCEPTED)
class SkillFunction(APIView):
    def get(self,request):
        all_skill = Skill.objects.all()
        return Response({"data":list(all_skill.values())})
