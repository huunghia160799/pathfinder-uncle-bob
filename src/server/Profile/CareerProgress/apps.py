from django.apps import AppConfig


class CareerprogressConfig(AppConfig):
    name = 'CareerProgress'
