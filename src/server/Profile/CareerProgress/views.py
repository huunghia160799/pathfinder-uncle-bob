from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db.models import F,ExpressionWrapper,FloatField,Count
import pandas as pd
import json
from django.db.models.functions import Floor
from django.http import JsonResponse
from .models import CareerProgress#,Category
from Pathway.Career.models import Career
from Profile.User.models import User
from datetime import datetime
import math
class CareerProgressAction(APIView):
    def get(self,request,user_name_input,title_input):
        #get CareerProgress by id
        careerProgress = CareerProgress.objects.filter(user=user_name_input,career=title_input).values()
        if(careerProgress.exists()==False):
            print("here")
            return Response({"No CareerProgress matching user_name '{}' and Career title '{}'".format(user_name_input,title_input)})
        return Response({"Retrieving CareerProgress successfully": careerProgress[0]})
    def post(self,request):
        #adding new CareerProgress 
        CareerProgress_insert = {}
        if(CareerProgress.objects.filter(user=request.POST["user_name"],career=request.POST["career_title"]).values().exists()):
            return Response({"This Career Progress has been already created"})
        if(Career.objects.filter(title=request.POST["career_title"]).values().exists()==False):
            return Response({"This Career title does not exist"})
        if(User.objects.filter(user_name=request.POST["user_name"]).values().exists()==False):
            return Response({"This User does not exist"})
        career = Career.objects.get(title=request.POST["career_title"])
        user = User.objects.get(user_name=request.POST["user_name"])
        CareerProgress_insert['career'] = career
        CareerProgress_insert['user'] = user
        new_CareerProgress = CareerProgress.objects.create(**CareerProgress_insert)
        del CareerProgress_insert['career']
        del CareerProgress_insert['user']
        CareerProgress_insert['career_title'] = request.POST["career_title"]
        CareerProgress_insert['user_name'] = request.POST["user_name"]
        return JsonResponse({"Creating CareerProgress successfully":CareerProgress_insert})
    def put(self,request):
        CareerProgress_insert = {}
        if(CareerProgress.objects.filter(user=request.POST["user_name"],career=request.POST["career_title"]).values().exists()==False):
            return Response({"Non-existing CareerProgress"})
        career = Career.objects.get(title=request.POST["career_title"])
        user = User.objects.get(user_name=request.POST["user_name"])
        CareerProgress_insert['career'] = career
        CareerProgress_insert['user'] = user
        match_CareerProgress = CareerProgress.objects.get(user=request.POST["user_name"],career=request.POST["career_title"])
        match_CareerProgress.career = CareerProgress_insert['career']
        match_CareerProgress.user = CareerProgress_insert['user']
        match_CareerProgress.save()
        del CareerProgress_insert['career']
        del CareerProgress_insert['user']
        CareerProgress_insert['career_title'] = request.POST["career_title"]
        CareerProgress_insert['user_name'] = request.POST["user_name"]
        return JsonResponse({"Updating CareerProgress successfully":CareerProgress_insert})
    def delete(self,request,user_name_input,title_input):
        if(CareerProgress.objects.filter(user=user_name_input,career=title_input).values().exists()==False):
            return Response({"Non-existing CareerProgress"})
        CareerProgress_delete = CareerProgress.objects.get(user=user_name_input,career=title_input)
        CareerProgress_delete.delete()
        return Response({"Deleting CareerProgress with user_name {} and Career_title {} successfully".format(user_name_input,title_input)})
# Create your views here.
