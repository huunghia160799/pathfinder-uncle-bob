from django.contrib import admin

from .models import CareerProgress

admin.site.register(CareerProgress)
# Register your models here.
