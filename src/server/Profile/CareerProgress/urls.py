from django.urls import path

from .views import CareerProgressAction


app_name = "CareerProgress"

# app_name will help us do a reverse look-up latter.
urlpatterns = [
    #api getting ad with id_ad params
    path('career_progress/<str:user_name_input>/<str:title_input>/',CareerProgressAction.as_view()),
    path('career_progress/',CareerProgressAction.as_view())
]