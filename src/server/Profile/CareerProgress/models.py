from django.db import models

from Pathway.Career.models import Career
from Profile.User.models import User
class CareerProgress(models.Model):
    class Meta:
        unique_together = (('user', 'career'),)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    career = models.ForeignKey(Career, on_delete=models.CASCADE)
# Create your models here.
# Create your models here.
