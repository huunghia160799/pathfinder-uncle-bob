from django.urls import path

from .views import UserAction,verify_user


app_name = "User"

# app_name will help us do a reverse look-up latter.
urlpatterns = [
    #api getting ad with id_ad params
    path('users/<str:user_name_input>/',UserAction.as_view(), name='user_action'),
    path('users/',UserAction.as_view()),
    path('users/verify/<str:user_name_input>/',verify_user.as_view())
]