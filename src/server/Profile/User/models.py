from django.db import models

# Create your models here.
class User(models.Model):
  # ids = models.IntegerField(primary_key=True)
  user_name = models.CharField(primary_key=True,max_length=255)
  github_username = models.CharField(max_length=255,null=True)
  name = models.CharField(max_length=255,null=True)
  dob = models.CharField(max_length=255,null=True)
  gender = models.CharField(max_length=255,null=True)
  email = models.CharField(max_length=255,null=True)
  password = models.CharField(max_length=255,null=True)