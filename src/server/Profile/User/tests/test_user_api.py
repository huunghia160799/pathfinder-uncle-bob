from django.test import TestCase
from django.urls import reverse
from Profile.User.views import UserAction

from rest_framework.test import APIClient
from rest_framework import status

CREATE_USER_URL = reverse('User:user_action', kwargs={'user_name_input': 'Test name'})


def create_user(**params):
    return get_user_model().objects.create_user(**params)


class PublicUserAPITests(TestCase):
    """ Test the users API (Public) """

    def setUp(self):
        self.client = APIClient()

    def test_create_valid_user_success(self):
        """ Test creating user with valid payload is successful """
        payload = {
            'user_name': 'test',
            'password': 'nghia123',
            'name': 'Test name',
            'dob': '16/07/1999',
            'gender': 'Male',
            'email': 'test@pathfinder.com',
            'github_username': 'nghia_git'
        }

        res = self.client.post(CREATE_USER_URL, payload)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
