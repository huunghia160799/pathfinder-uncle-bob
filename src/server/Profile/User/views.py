from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db.models import F, ExpressionWrapper, FloatField, Count
import pandas as pd
import json
from django.db.models.functions import Floor
from django.http import JsonResponse
from rest_framework import status
from .models import User  # ,Category
from datetime import datetime
import math
from django.core import serializers


class UserAction(APIView):
    def get(self, request, user_name_input):
        # get user by id
        print("User Name:", user_name_input)
        user = User.objects.filter(user_name=user_name_input).values()
        if(user.exists() == False):
            print("here")
            return Response({"data": []})
        return Response({"data": user[0]})

    def post(self, request, user_name_input):
        # adding new user
        user_request = request.POST
        user_insert = {}
        user_insert['user_name'] = request.POST["user_name"]
        if(User.objects.filter(user_name=user_insert['user_name']).values().exists()):
            return Response({"This user_name has been used by someone"}, status=status.HTTP_400_BAD_REQUEST)
        user_insert['github_username'] = request.POST["github_username"]
        user_insert['name'] = request.POST["name"]
        user_insert['dob'] = request.POST["dob"]
        user_insert['gender'] = request.POST["gender"]
        user_insert['email'] = request.POST["email"]
        user_insert['password'] = request.POST["password"]
        new_user = User.objects.create(**user_insert)
        return Response({"Creating User successfully": user_insert}, status=status.HTTP_201_CREATED)

    def put(self, request):
        user_insert = {}
        user_insert['github_username'] = request.POST["github_username"]
        user_insert['name'] = request.POST["name"]
        user_insert['dob'] = request.POST["dob"]
        user_insert['gender'] = request.POST["gender"]
        user_insert['email'] = request.POST["email"]
        user_insert['password'] = request.POST["password"]
        if(User.objects.filter(user_name=request.POST["user_name"]).values().exists() == False):
            return Response({"Non-existing user"}, status=status.HTTP_400_BAD_REQUEST)
        match_user = User.objects.get(user_name=request.POST["user_name"])
        user_insert['user_name'] = match_user.user_name
        match_user.github_username = user_insert['github_username']
        match_user.name = user_insert['name']
        match_user.dob = user_insert['dob']
        match_user.gender = user_insert['gender']
        match_user.email = user_insert['email']
        match_user.password = user_insert['password']
        match_user.save()
        return Response({"Updating User successfully": user_insert}, status=status.HTTP_201_CREATED)

    def delete(self, request, user_name_input):
        print("User name for deleting:", user_name_input)
        if(User.objects.filter(user_name=user_name_input).values().exists() == False):
            return Response({"Non-existing user"})
        user_delete = User.objects.get(user_name=user_name_input)
        user_delete.delete()
        return Response({"Deleting User {} successfully".format(user_name_input)}, status=status.HTTP_202_ACCEPTED)
    # def delete(self,request):
# Create your views here.


class verify_user(APIView):
    def post(self, request, user_name_input):
        password_request = request.POST["password"]
        print(password_request)
        if(User.objects.filter(user_name=user_name_input).values().exists() == False):
            return Response({"Non-existing user"}, status=status.HTTP_400_BAD_REQUEST)
        user = User.objects.filter(user_name=user_name_input)
        if(password_request != user[0].password):
            return Response({"Wrong password"}, status=status.HTTP_400_BAD_REQUEST)
        #user = serializers.serialize('json',user.values())
        response = JsonResponse({"Log in successfully": list(user.values())})
        response["Access-Control-Allow-Origin"] = "*"
        response["Access-Control-Allow-Methods"] = "GET, OPTIONS"
        response["Access-Control-Max-Age"] = "1000"
        response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"
        return response
