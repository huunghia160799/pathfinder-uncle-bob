from django.contrib import admin

from .models import SkillProgress

admin.site.register(SkillProgress)
# Register your models here.
