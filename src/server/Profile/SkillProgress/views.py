from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db.models import F,ExpressionWrapper,FloatField,Count
import pandas as pd
import json
from django.db.models.functions import Floor
from django.http import JsonResponse
from .models import SkillProgress#,Category
from Pathway.Skill.models import Skill
from Profile.User.models import User
from datetime import datetime
import math
class SkillProgressAction(APIView):
    def get(self,request,user_name_input,title_input):
        #get SkillProgress by id
        skillProgress = SkillProgress.objects.filter(user=user_name_input,skill=title_input).values()
        if(skillProgress.exists()==False):
            print("here")
            return Response({"No SkillProgress matching user_name '{}' and skill title '{}'".format(user_name_input,title_input)})
        return Response({"Retrieving SkillProgress successfully": skillProgress[0]})
    def post(self,request):
        #adding new SkillProgress 
        SkillProgress_insert = {}
        if(SkillProgress.objects.filter(user=request.POST["user_name"],skill=request.POST["skill_title"]).values().exists()):
            return Response({"This Skill Progress has been already created"})
        SkillProgress_insert['completed'] = request.POST["completed"]
        if(Skill.objects.filter(title=request.POST["skill_title"]).values().exists()==False):
            return Response({"This Skill title does not exist"})
        if(User.objects.filter(user_name=request.POST["user_name"]).values().exists()==False):
            return Response({"This User does not exist"})
        skill = Skill.objects.get(title=request.POST["skill_title"])
        user = User.objects.get(user_name=request.POST["user_name"])
        SkillProgress_insert['skill'] = skill
        SkillProgress_insert['user'] = user
        new_SkillProgress = SkillProgress.objects.create(**SkillProgress_insert)
        del SkillProgress_insert['skill']
        del SkillProgress_insert['user']
        SkillProgress_insert['skill_title'] = request.POST["skill_title"]
        SkillProgress_insert['user_name'] = request.POST["user_name"]
        return JsonResponse({"Creating SkillProgress successfully":SkillProgress_insert})
    def put(self,request):
        SkillProgress_insert = {}
        SkillProgress_insert['completed'] = request.POST["completed"]
        if(SkillProgress.objects.filter(user=request.POST["user_name"],skill=request.POST["skill_title"]).values().exists()==False):
            return Response({"Non-existing SkillProgress"})
        skill = Skill.objects.get(title=request.POST["skill_title"])
        user = User.objects.get(user_name=request.POST["user_name"])
        SkillProgress_insert['skill'] = skill
        SkillProgress_insert['user'] = user
        match_SkillProgress = SkillProgress.objects.get(user=request.POST["user_name"],skill=request.POST["skill_title"])
        match_SkillProgress.completed = SkillProgress_insert['completed']
        match_SkillProgress.skill = SkillProgress_insert['skill']
        match_SkillProgress.user = SkillProgress_insert['user']
        match_SkillProgress.save()
        del SkillProgress_insert['skill']
        del SkillProgress_insert['user']
        SkillProgress_insert['skill_title'] = request.POST["skill_title"]
        SkillProgress_insert['user_name'] = request.POST["user_name"]
        return JsonResponse({"Updating SkillProgress successfully":SkillProgress_insert})
    def delete(self,request,user_name_input,title_input):
        if(SkillProgress.objects.filter(user=user_name_input,skill=title_input).values().exists()==False):
            return Response({"Non-existing SkillProgress"})
        SkillProgress_delete = SkillProgress.objects.get(user=user_name_input,skill=title_input)
        SkillProgress_delete.delete()
        return Response({"Deleting SkillProgress with user_name {} and skill_title {} successfully".format(user_name_input,title_input)})
# Create your views here.
