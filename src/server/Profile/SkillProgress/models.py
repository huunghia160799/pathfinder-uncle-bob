from django.db import models

from Pathway.Skill.models import Skill
from Profile.User.models import User
class SkillProgress(models.Model):
    class Meta:
        unique_together = (('user', 'skill'),)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    skill = models.ForeignKey(Skill, on_delete=models.CASCADE)
    completed = models.CharField(max_length=255)
# Create your models here.
