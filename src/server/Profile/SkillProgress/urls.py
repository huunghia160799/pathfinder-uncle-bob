from django.urls import path

from .views import SkillProgressAction


app_name = "SkillProgress"

# app_name will help us do a reverse look-up latter.
urlpatterns = [
    #api getting ad with id_ad params
    path('skill_progress/<str:user_name_input>/<str:title_input>/',SkillProgressAction.as_view()),
    path('skill_progress/',SkillProgressAction.as_view())
]