from django.apps import AppConfig


class SkillprogressConfig(AppConfig):
    name = 'SkillProgress'
